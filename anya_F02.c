#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>

void format_log(char *cmdname, char *type)
{
    char log[1024], date[30];
    char cwd[1024];
    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    getcwd(cwd, sizeof(cwd));

    sprintf(log, "%s::%d %d %d-%d:%d:%d::%s::%s", type, timeinfo->tm_mday,
            timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
            timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, cmdname, cwd);
    FILE *fptr;

    fptr = fopen("/home/osboxes/hayolongapain_F02.log", "a");

    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr, "%s\n", log);
    fclose(fptr);
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    res = lstat(path, stbuf);

    if (res == -1)
        return -errno;

    FILE *fptr;

    fptr = fopen("/home/osboxes/hayolongapain_F02.log", "a");

    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    // fprintf(fptr, "GETATTR\n");
    fclose(fptr);
    // format_log("GETATTR", "INFO");

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(path);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));
        if (strstr(de->d_name, "Animeku_"))
        {
            char result[300], initial[300], init_name[300], mode[300];
            result[0] = '\0';
            initial[0] = '\0';
            init_name[0] = '\0';
            strcpy(init_name, de->d_name);

            // printf("%s\n", de->d_name);
            // sprintf(initial, "%s/%s", path, de->d_name);
            encode(de->d_name + 8);
            if (strcmp(de->d_name, init_name) == 0)
            {
                mode[0] = '\0';
                strcpy(mode, "terdecode");
                decode(de->d_name + 8);
            }
            else
            {
                mode[0] = '\0';
                strcpy(mode, "terencode");
            }
            sprintf(result, "%s/%s", path, de->d_name);

            FILE *fptr;

            fptr = fopen("Wibu.log", "a");

            if (fptr == NULL)
            {
                printf("Error!");
                exit(1);
            }
            fprintf(fptr, "RENAME %s %s -> %s\n", mode, initial, result);
            fclose(fptr);
        }
        if (strstr(de->d_name, "IAN_"))
        {
            /* code */
            char result[300], initial[300], init_name[300], mode[300];
            result[0] = '\0';
            initial[0] = '\0';
            init_name[0] = '\0';
            strcpy(init_name, de->d_name);
            vigenereencode(de->d_name + 4);
            FILE *fptr;

            fptr = fopen("/home/osboxes/hayolongapain_F02.log", "a");

            if (fptr == NULL)
            {
                printf("Error!");
                exit(1);
            }
            fprintf(fptr, "READDIR\n");
            fclose(fptr);
        }

        // printf("%s", de->d_name);
        //  strcat(de->d_name, "_animeku");
        //   encode(de->d_name);
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (filler(buf, de->d_name, &st, 0))
            break;
    }
    // bikin seg fault, ngaa ngerti tbh
    //  if (strstr(de->d_name, "nam_do-saq_"))
    //  {
    //      printf("dir abnormal\n");
    //      /* code */
    //  }
    //  else
    //  {
    //      printf("dir normal\n");
    //  }

    closedir(dp);
    // format_log("READDIR", "INFO");
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    (void)fi;

    fd = open(path, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    FILE *fptr;

    fptr = fopen("/home/osboxes/hayolongapain_F02.log", "a");

    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr, "READ\n");
    fclose(fptr);
    format_log("READ", "INFO");
    return res;
}

void encode(char *name)
{
    char result[300];

    for (int i = 0; i < strlen(name); i++)
    {
        /* code */
        if (name[i] > 96 && name[i] < 123)
        {
            // rot13
            if (name[i] > 109)
            {
                name[i] = name[i] - 13;
            }
            else
            {
                name[i] = name[i] + 13;

                /* code */
            }
        }
        if (name[i] > 64 && name[i] < 91)
        {
            // atbash
            name[i] = 'Z' + 'A' - name[i];
        }
        // printf("%s\n", name);
    }
}

void decode(char *name)
{
    char result[300];

    for (int i = 0; i < strlen(name); i++)
    {
        /* code */
        if (name[i] > 96 && name[i] < 123)
        {
            // rot13
            if (name[i] > 109)
            {
                name[i] = name[i] - 13;
            }
            else
            {
                name[i] = name[i] + 13;

                /* code */
            }
        }
        else if (name[i] > 64 && name[i] < 91)
        {
            // atbash
            name[i] = 'Z' + 'A' - name[i];
        }
        printf("%s\n", name);
    }
}

void vigenereencode(char *text)
{
    char keytxt[] = "INNUGANTENG";
    int textln = strlen(text);
    char encryptedMsg[textln];
    int i;
    for (i = 0; i < textln; ++i)
    {

        encryptedMsg[i] = ((text[i] + keytxt[i]) % 26) + 65;
    }
    encryptedMsg[i] = '\0';
    // strcpy(text, encryptedMsg);
    // return text;
}

void vigeneredecode(char *text)
{
    char keytxt[] = "INNUGANTENG";
    int textln = strlen(text);
    char decryptedMsg[textln];
    int i;
    for (i = 0; i < textln; ++i)
    {

        decryptedMsg[i] = (((text[i] - keytxt[i]) + 26) % 26) + 65;
    }
    decryptedMsg[i] = '\0';
    // strcpy(text, decryptedMsg);

    // return text;
}

static int xmp_rmdir(char *path)
{
    format_log("RMDIR", "WARNING");

    char cwd[1024];
    getcwd(cwd, sizeof(cwd));
    printf("Current working dir: %s\n", cwd);
    strcat(cwd, "/");
    strcat(cwd, path);
    rmdir(cwd);
    FILE *fptr;

    fptr = fopen("/home/osboxes/hayolongapain_F02.log", "a");

    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr, "RMDIR\n");
    fclose(fptr);
}

static int xmp_rename(char *name, const char *newname)
{

    FILE *fptr;

    fptr = fopen("/home/osboxes/hayolongapain_F02.log", "a");

    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr, "RENAME\n");
    fclose(fptr);
    printf("hallo\n");
    format_log("chDIR", "INFO");
}
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
    .rmdir = xmp_rmdir,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}