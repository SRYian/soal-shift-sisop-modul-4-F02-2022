# soal-shift-sisop-modul-4-F02-2022

Sisop modul 4

# Group

1. **Billy Brianto**(5025201080)
2. Amsal Herbert(5025201182)
3. Aqil Ramadhan Hadiono(5025201261)

## 1 Anya

Untuk soal ini root harus dimount ke /home/[USER]/Documents. setelah itu, semua direktori dengan awalan Anime\_ akan diencode dnegan atbash dan rot13 cypher

### Atbash

Atbash cipher adalah Cipher yang tidak menggunakan key. Ide untuk atbash cypher adalah membalik A menjadi Z, B menjadi Y, dan seterusnya

### Rot13

Rot13 seperti namanya adalah Cipher yang menggunakan 26 karakter latin. Untuk cipher ini tiap alfabet diputar/diganti ke angaka ke 13 setelahnya.

### Encode/Decode

```c
void encode(char *name)
{
    char result[300];

    for (int i = 0; i < strlen(name); i++)
    {
        /* code */
        if (name[i] > 96 && name[i] < 123)
        {
            // rot13
            if (name[i] > 109)
            {
                name[i] = name[i] - 13;
            }
            else
            {
                name[i] = name[i] + 13;

                /* code */
            }
        }
        if (name[i] > 64 && name[i] < 91)
        {
            // atbash
            name[i] = 'Z' + 'A' - name[i];
        }
        // printf("%s\n", name);
    }
}

void decode(char *name)
{
    char result[300];

    for (int i = 0; i < strlen(name); i++)
    {
        /* code */
        if (name[i] > 96 && name[i] < 123)
        {
            // rot13
            if (name[i] > 109)
            {
                name[i] = name[i] - 13;
            }
            else
            {
                name[i] = name[i] + 13;

                /* code */
            }
        }
        else if (name[i] > 64 && name[i] < 91)
        {
            // atbash
            name[i] = 'Z' + 'A' - name[i];
        }
        printf("%s\n", name);
    }
}
```

Proses encode dan decode pada soal ini dialkukan dalam satu fungsi untuk decode dan encode. Tiap fungsi akan memisahkan antara huruf besar dan kecil dan mengencode/decode dengan cypher yang sesuai.

### Substring

```c
if (strstr(de->d_name, "Animeku_"))
        {
            char result[300], initial[300], init_name[300], mode[300];
            result[0] = '\0';
            initial[0] = '\0';
            init_name[0] = '\0';
            strcpy(init_name, de->d_name);

            // printf("%s\n", de->d_name);
            // sprintf(initial, "%s/%s", path, de->d_name);
            encode(de->d_name + 8);
            if (strcmp(de->d_name, init_name) == 0)
            {
                mode[0] = '\0';
                strcpy(mode, "terdecode");
                decode(de->d_name + 8);
            }
            else
            {
                mode[0] = '\0';
                strcpy(mode, "terencode");
            }
            sprintf(result, "%s/%s", path, de->d_name);

            FILE *fptr;

            fptr = fopen("Wibu.log", "a");

            if (fptr == NULL)
            {
                printf("Error!");
                exit(1);
            }
            fprintf(fptr, "RENAME %s %s -> %s\n", mode, initial, result);
            fclose(fptr);
        }
```

Untuk mendeteksi folder yang memiliki substring Animeku\_, dapat ditambahakan blok if seperti diatas dalam looping while readdir.
Untuk subproblem selanjutnya, mendeteksi apakah direktori yang direname menjaid tidak terencode dapat dilakukan dengan menyimpan string awal nama dan membandingkannya dengan nama saat ini. Jika sma, lakukan decode.
Dalam subproblem terakhir, diminta untuk membuat folder Wibu.log untuk menyimpan semua file yang anamanya telah diubah. Untuk itu, dilakkan printf ke file string yang berupa nama sebelum, sesudah, dan mode.

## 2 Innu

Untuk soal ini, jika ada sebuah direktori dengan nama IAN\_[nama], direktori tersebut akan diencode dengan vigenere cypher dengan key INNUGANTENG.

### Vigenere Cypher

Vigenere cyoher adalah cara unutk encoding text menggunakan polypalphabetic substitution(beberapa substitutsi). Vigenere cypher menggunakan tabel yang berisi alfabet yang ditulis 26 kali. Tiap alfabet akan di shift secara siklik. Alfabet yang dugnakan ini bergantung kepada suatu key.

### Encode/Decode

```c
const char *vigenereencode(char *text)
{
    char keytxt[] = "INNUGANTENG";
    int textln = strlen(text);
    char encryptedMsg[textln];
    int i;
    for (i = 0; i < textln; ++i)
    {

        encryptedMsg[i] = ((text[i] + keytxt[i]) % 26) + 65;
    }
    encryptedMsg[i] = '\0';
    strcpy(text, encryptedMsg);
    return text;
}

const char *vigeneredecode(char *text)
{
    char keytxt[] = "INNUGANTENG";
    int textln = strlen(text);
    char decryptedMsg[textln];
    int i;
    for (i = 0; i < textln; ++i)
    {

        decryptedMsg[i] = (((text[i] - keytxt[i]) + 26) % 26) + 65;
    }
    decryptedMsg[i] = '\0';
    strcpy(text, decryptedMsg);

    return text;
}
```

Untuk encode dan decode dapat dilakukan dengan iterasi tiap karakter dalam string dan menambahkannya dengan karakter key. Hasilnya kemudian akan dimodulo 26 dan ditambah huruf A.

```c
if (strstr(de->d_name, "IAN_"))
        {
            /* code */
            char result[300], initial[300], init_name[300], mode[300];
            result[0] = '\0';
            initial[0] = '\0';
            init_name[0] = '\0';
            strcpy(init_name, de->d_name);
            vigenereencode(de->d_name + 4);
            FILE *fptr;

            fptr = fopen("/home/osboxes/hayolongapain_F02.log", "a");

            if (fptr == NULL)
            {
                printf("Error!");
                exit(1);
            }
            fprintf(fptr, "READDIR\n");
            fclose(fptr);
        }
```

Untuk mendeteksi folder yang memiliki substring IAN\_, dapat ditambahakan blok if seperti diatas dalam looping while readdir. Selanjutnya tiap file yang sesuai akan diencode.

### Log & Formatting

Pada subproblem selanjutnya, diminta untuk membuat log untuk menyimpan history sistem call. Namun, log dibagi 2 level: debug dan warning.

```c
void format_log(char *cmdname, char *type)
{
    char log[1024], date[30];
    char cwd[1024];
    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    getcwd(cwd, sizeof(cwd));

    sprintf(log, "%s::%d %d %d-%d:%d:%d::%s::%s", type, timeinfo->tm_mday,
            timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
            timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, cmdname, cwd);
    FILE *fptr;

    fptr = fopen("/home/osboxes/hayolongapain_F02.log", "a");

    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr, "%s\n", log);
    fclose(fptr);
}
```

Untuk membuat log, dibuat sebuah fungsi bernama format_log. fungsi ini menerima dua parameter: cmdname untuk Command apa yang dilakukan dan type, type command yang dilakukan: debug atau warning. Untuk mencapai formatting yang sesuai, digunakan library time.h untuk mendapatkan waktu dan tanggal. Selanjutnya fungsi ini diletakan di tiap fungsi yang sesuai(read, rename, etc.) dengan parameter yang sesuai untuk mendapatkan log tiap command.

## 3 Ishan

```c
 if (strstr(de->d_name, "nam_do-saq_"))
    {
        printf("dir abnormal\n");
        /* code */
    }
    else
    {
        printf("dir normal\n");
    }
```

Untuk nomor 3, deteksi substring nam*do-saq* dapat dilakukan dengan srestr.
Pengembalian encoding dan eksensi tidak jelas bagaimana mendapatkannya.

## Masalah dan Kendala

### 1

![nomer1](https://gitlab.com/uploads/-/system/user/10950063/f31cdcb9da941b79e72b8426897b12ef/image.png)
Kurang mengerti Fuse, soal yang kurang jelas memberikan instruksi apakah rename harus diimplementasikan sendiri atau rename sudah diilakukan terlebih dahulu. Kelompok tidak aktif.

### 2

![nomer2](https://gitlab.com/uploads/-/system/user/10950063/90ed6bf34b924bce78b4f1ef67c194cb/image.png)

Soal sedikit ambigu dalam menyatakan pembuatan log.

### 3

![nomer3](https://gitlab.com/uploads/-/system/user/10950063/3a7f71b84cb5a6f83480cec383e1fab2/image.png)

Soal sangat ambigu pada bagian d dan e
